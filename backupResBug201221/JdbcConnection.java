import java.sql.*;

public class JdbcConnection {


    private String url = "jdbc:mysql://localhost:3306/test"; //chemin vers la base [nom]
    private String userName = "root";
    private String mdp = "";

    public Connection openConnection() {
        Connection con = null;
        try {
            con = DriverManager.getConnection(url, userName, mdp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return con;
    }

}
