//Import des classes nécessaires au traitement des fichiers

import java.sql.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;


public class Main {

    public static void main(String[] args) {


/*
        JdbcConnection jdbc = new JdbcConnection();
        Connection con = jdbc.openConnection();


        try {
            PreparedStatement st = con;
            ResultSet rs = st.executeQuery();
            while(rs.next()) {
                System.out.println(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

*/
        ReadFile rf = new ReadFile();
        //Boucle while pour assurer les tests.
        while (true) {

            System.out.println("Indiquer le chemin UNC fichier dont les données sont à introduire en base de données : ");
            Scanner sc = new Scanner(System.in);
            String input = sc.nextLine();
            //on récupère le chemin renseigné dans une variable de type Path afin de pouvoir en caractériser l'objet de destination.
            if (rf.isValid(input)) {
                ArrayList<String> bf = rf.readFile(input);
                MusicObjectsTools mo = new MusicObjectsTools();
                ArrayList<BandeOriginale> bos = mo.collectBo(bf);
                for (BandeOriginale bo : bos) {
                    System.out.println(bo.getTitle());

                }


            }

           /* if (Files.exists(path)) {
                if (Files.isDirectory(path)) {
                    System.out.println("L'input donné est incorrect, il s'agit ici d'un dossier.");
                } else {
                    System.out.println("Le fichier existe bien");

                    // On lis le fichier
                    try {

                        FileReader fr = new FileReader(input);
                        BufferedReader br = new BufferedReader(fr);
                        StringBuffer sb = new StringBuffer();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }*/
           /* fr.close();
            System.out.println("Contenu du fichier : ");
            System.out.println(sb.toString());
        } catch(IOException e)
        {
            e.printStackTrace();
        }*/
            break;
        }
    }


}