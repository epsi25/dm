import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ReadFile {


    public boolean isValid(String input) {

        Path path = Paths.get(input);
        if (Files.exists(path)) {
            if (Files.isDirectory(path)) {
                System.out.println("L'input donné est incorrect, il s'agit ici d'un dossier.");
                return false;

            } else {
                System.out.println("Le fichier existe bien");
                return true;
            }

        } else {
            return false;
        }
    }


    public ArrayList<String> readFile(String path) {

        // On lis le fichier
        StringBuffer sb = new StringBuffer();
        ArrayList<String> data = new ArrayList<>();
        try {
            FileReader fr = new FileReader(path);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                     data.add(line); // va ensuite être splité en plusieurs objets

            }
            fr.close();
            System.out.println("Contenu du fichier : ");
            System.out.println(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }


}
