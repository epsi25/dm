public class BandeOriginale extends Musique {

    private String movieTitle;

    public BandeOriginale(String title, String artists, int year, String movieTitle) {
        super(Type.BANDE_ORIGINALE, artists, title, year);
        this.movieTitle = movieTitle;

    }


}
