public abstract class  Musique {

    private Type type;
    private String artists;
    private String title;
    private int year;


    public Musique(Type type, String artists, String title, int year) {
        this.type = type;
        this.artists = artists;
        this.title = title;
        this.year = year;

    }

    public Type getType() {
        return type;
    }

    public String getArtistes() {
        return artists;
    }

    public void setArtistes() {
        this.artists = artists;
    }

    public Type gettype() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Musique{" +
                "type=" + type +
                ", artistes='" + artists + '\'' +
                ", title='" + title + '\'' +
                ", year=" + year +
                '}';
    }
}
