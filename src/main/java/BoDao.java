import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class BoDao {

    public void InsertBo(BandeOriginale Bo) {


        try {
            JdbcConnection jd = new JdbcConnection();
            PreparedStatement ps = jd.openConnection().prepareStatement("INSERT INTO bandeoriginale(artist,title,year,movieTitle) VALUES (?,?,?,?);");
            ps.setString(1, Bo.getArtistes());
            ps.setString(2, Bo.getTitle());
            ps.setInt(3, Bo.getYear());
            ps.setString(4, Bo.getMovieTitle());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public ArrayList<BandeOriginale> getTableBandeOriginale(){

        ArrayList<BandeOriginale> bos = new ArrayList<>();

        try {
            JdbcConnection con = new JdbcConnection();
            PreparedStatement ps = con.openConnection().prepareStatement("SELECT * FROM bandeoriginale;");

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                BandeOriginale bo = new BandeOriginale(rs.getString("artist"), rs.getString("title"), rs.getInt("year"), rs.getString("movieTitle"));
                System.out.println(bo.toString());
                bos.add(bo);
            }
            ps.close();

        }catch (SQLException e){
            e.printStackTrace();
        }
return bos;

    }



















}
    
    
    
    
    
    
    
    
    
    
    
    
    
    

