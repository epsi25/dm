import java.sql.*;

public class JdbcConnection {


    private String url = "jdbc:mysql://localhost:3306/oeuvresmusicales"; //chemin vers la base [nom]
    private String userName = "root";
    private String mdp = "";

    public Connection openConnection() {
        Connection con = null;
        try {
            con = DriverManager.getConnection(url, userName, mdp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return con;
    }


    public void closeConnection(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


/*
        JdbcConnection jdbc = new JdbcConnection();
        Connection con = jdbc.openConnection();


        try {
            PreparedStatement st = con;
            ResultSet rs = st.executeQuery();
            while(rs.next()) {
                System.out.println(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

*/

}


