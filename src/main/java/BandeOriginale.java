public class BandeOriginale extends Musique {

    private String movieTitle;

    public BandeOriginale(String title, String artists, int year, String movieTitle) {
        super(Type.BANDE_ORIGINALE, artists, title, year);
        this.movieTitle = movieTitle;

    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }








}
