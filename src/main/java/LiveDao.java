import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class LiveDao {

    public void insertLive(AlbumLive Al) {
        try {
            JdbcConnection con = new JdbcConnection();
            PreparedStatement ps = con.openConnection().prepareStatement("INSERT INTO albumlive(artist,title,year,location) VALUES (?,?,?,?)");
            ps.setString(1, Al.getArtistes());
            ps.setString(2, Al.getTitle());
            ps.setInt(3, Al.getYear());
            ps.setString(4, Al.getLocation());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }





    public ArrayList<AlbumLive> getTableAlbumLive(){
        ArrayList<AlbumLive> als = new ArrayList<>();

        try {
            JdbcConnection con = new JdbcConnection();
            PreparedStatement ps = con.openConnection().prepareStatement("SELECT * FROM albumlive;");

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                AlbumLive al = new AlbumLive(rs.getString("artist"), rs.getString("title"), rs.getInt("year"), rs.getString("location"));
                System.out.println(al.toString());
                als.add(al);
            }
            ps.close();

        }catch (SQLException e){
            e.printStackTrace();
        }
        return als;


    }











}
