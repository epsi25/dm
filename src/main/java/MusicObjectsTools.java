import java.util.ArrayList;

public class MusicObjectsTools {


    public ArrayList<BandeOriginale> collectBo(ArrayList<String> data) {
        ArrayList<BandeOriginale> bos = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            try{
            String[] data1 = data.get(i).split(";");
            System.out.println(data1[0]);
            if (data1[0].toLowerCase().equals("bo")) {

                int year = Integer.parseInt(data1[3]);
                BandeOriginale bo = new BandeOriginale(data1[1], data1[2], year, data1[4]);
                bos.add(bo);
            }
            }catch(Exception e){
                System.out.println("Les données du fichiers sont incohérentes : " + data.get(i));
            }
        }
        return bos;

    }


    public ArrayList<AlbumLive> collectLive(ArrayList<String> data) {

        ArrayList<AlbumLive> lives = new ArrayList<>();
        System.out.println("on est ligne 9");
        for (int i = 0; i < data.size(); i++) {
            try {
                String[] data1 = data.get(i).split(";"); // On récupère un objet d'ArrayList à l'indice [i]
                System.out.println(data1[0]);
                if (data1[0].toLowerCase().equals("live")) { //en vue du traitement distinguant album live de BO.
                    int year = Integer.parseInt(data1[3]);
                    String location = data1[4];
                    AlbumLive live = new AlbumLive(data1[2], data1[3], year, data1[4]);
                    lives.add(live);
                }
            }catch(Exception e){
                    System.out.println("Les données du fichiers sont incohérentes : " + data.get(i));
                }
        }
        return lives;


    }


}
